mod client;
mod handlers;

use std::io;

use clap::{Parser, Subcommand};
use tokio::fs;

use client::FirehoseClient;
use handlers::{download, publish, stream};

#[derive(Debug, Parser)]
#[clap(name = "dstream", about = "Shodan firehose command line interface")]
pub struct Context {
    #[clap(short, long, default_value = "https://stream.shodan.io")]
    host: String,
    #[clap(long = "batch-size", default_value = "5")]
    batch_size: usize,
    #[clap(long = "lines")]
    lines: bool,
    #[clap(subcommand)]
    command: Command,
}

#[derive(Clone, Debug, Subcommand)]
enum Command {
    Download(download::Args),
    Publish(publish::Args),
    Stream,
}

async fn get_api_key() -> io::Result<String> {
    let key_path = {
        let mut api_key = dirs::home_dir().unwrap();
        api_key.push(".shodan");
        api_key.push("api_key");
        api_key
    };
    if key_path.exists() {
        return fs::read_to_string(key_path).await;
    }

    let key_path = {
        let mut api_key = dirs::home_dir().unwrap();
        api_key.push(".config");
        api_key.push("shodan");
        api_key.push("api_key");
        api_key
    };
    if key_path.exists() {
        return fs::read_to_string(key_path).await;
    }

    Err(io::Error::new(
        io::ErrorKind::NotFound,
        "`api_key` file not found at ~/.config/shodan",
    ))
}

#[tokio::main]
async fn main() {
    let api_key = match get_api_key().await {
        Ok(api_key) => api_key,
        Err(err) => panic!("{}", err),
    };

    let ctx = Context::parse();
    let command = ctx.command.clone();
    let client = FirehoseClient::new(api_key);
    match command {
        Command::Download(args) => download::handler(client, ctx, args).await,
        Command::Publish(args) => publish::handler(client, ctx, args).await,
        Command::Stream => stream::handler(client).await,
    }
}
